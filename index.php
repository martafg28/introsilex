<?php
 require_once __DIR__.'/vendor/autoload.php';

$app = new Silex\Application();

//aplicacion para depurar
$app['debug'] = true;

// código de los controladores
//ejemplo sencillo. cuando reciba hola realiza una funcion sin nombre.
//'hola' es la ruta.
$app->get('/hola', function(){
	return '<h1>Hola desde Silex</h1>';
});

//otro controlador con parametros. cuando reciba /hola/nombe realiza la funcion sin nombre con el paramentro
$app->get('/hola/{nombre}', function($nombre) use ($app){
	return '<h1>Hola <strong>'.$app->escape($nombre).'</strong></h1>';
});
 
$app->run();


?>